﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticDemo.Domain
{
    public class Author
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset BirthDate { get; set; }
    }
}
