﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticDemo.Domain
{
    public class Book
    {
        public Guid Id { get; set; }
        public List<Author> Authors { get; set; }
        public List<Tag> Tags { get; set; }
        public DateTimeOffset PublishedAt { get; set; }
        public string Title { get; set; }
    }
}
