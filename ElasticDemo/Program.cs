﻿using Nest;
using System;

namespace ElasticDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var settings = new ConnectionSettings(new Uri("http://localhost:9200"))
                .DefaultIndex("store-*")
                .DefaultTypeName("doc");
        }


        private static void PutIndexTemplate(ElasticClient client)
        {
            var putIndexTemplateResponse = client.PutIndexTemplate("storetemplate", t => t
                .IndexPatterns("store-*")
                .Mappings(m => m
                    .Map<Product>(tm => tm
                        .AutoMap()
                    )
                )
            );
        }
    }
}
