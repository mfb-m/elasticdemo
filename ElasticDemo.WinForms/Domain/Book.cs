﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticDemo.WinForms.Domain
{
    public class Book
    {
        [Keyword]
        public Guid Id { get; set; }

        [Text]
        public string Title { get; set; }

        [Date]
        public DateTimeOffset CreatedAt { get; set; }

        [Text]
        public string Content { get; set; }

        [Nested]
        public List<Author> Authors { get; set; }

        [Completion]
        public CompletionField Suggest
        {
            get => new CompletionField { Input = new string[] { Title } };
        }
    }
}
