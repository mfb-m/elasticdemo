﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticDemo.WinForms.Domain
{
    public class Author
    {
        [Text]
        public string Name { get; set; }
    }
}
