﻿using Bogus;
using ElasticDemo.WinForms.Domain;
using Nest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElasticDemo.WinForms
{
    public partial class Form1 : Form
    {
        private ElasticClient ElasticClient { get; set; }

        public Form1()
        {
            InitializeComponent();
            CreateClient();
            CreateIndex();
            FillDatabase();
            //GetAllBooks();
            //GetBooksDateRange();
        }


        public void CreateClient()
        {
            var settings = new ConnectionSettings(new Uri("http://localhost:9200"))
                .DefaultIndex("books-*")
                .DefaultTypeName("doc");

            ElasticClient = new ElasticClient(settings);
        }

        public void CreateIndex()
        {
            var putIndexTemplateResponse = ElasticClient.PutIndexTemplate("storetemplate", t => t
                        .IndexPatterns("books-*")
                        .Mappings(m => m
                            .Map<Book>(tm => tm
                                .AutoMap()
                                )
                            )
                        );
        }


        private static List<Book> GetBooks(int number)
        {
            var testProduct = new Faker<Book>()
                .RuleFor(o => o.Id, (f, e) => Guid.NewGuid())
                .RuleFor(o => o.Content, (f, e) => f.Lorem.Paragraph())
                .RuleFor(o => o.Title, (f, e) => f.Commerce.ProductName())
                .RuleFor(o => o.CreatedAt, (f, e) => f.Date.BetweenOffset(DateTimeOffset.UtcNow.AddYears(-1), DateTimeOffset.UtcNow.AddYears(2)));
            return testProduct.Generate(number);
        }

        public void FillDatabase()
        {
            var books = GetBooks(1000);
            var reponse = ElasticClient.IndexMany(books);
            var productsByYear = books.GroupBy(o => o.CreatedAt.Year).Select(o => new { Year = o.Key, Values = o.ToList() });
            foreach (var item in productsByYear)
            {
                var response = ElasticClient.IndexMany(item.Values, $"books-{item.Year}");
            }

        }

        /// <summary>
        /// Pro defecto devuelve 10
        /// </summary>
        /// <returns></returns>
        public List<Book> GetAllBooks()
        {
            var response = ElasticClient.Search<Book>();
            return response.Hits.Select(o => o.Source).ToList();
        }

        /// <summary>
        /// bool query = where sql
        /// </summary>
        /// <returns></returns>
        public List<Book> GetBooksDateRange()
        {
            var response = ElasticClient.Search<Book>(search => search
                    .Query(query => query
                        .Bool(boo => boo
                            .Filter(filter => filter
                                .DateRange(date => date
                                        .Field(field => field.CreatedAt)
                                        .GreaterThanOrEquals(DateTimeOffset.UtcNow.AddYears(-1).ToString("dd/MM/yyyy"))
                                        .LessThanOrEquals(DateTimeOffset.UtcNow.ToString("dd/MM/yyyy"))
                                        .Format("dd/MM/yyyy")
                                    )
                                )
                           )
                        )
                    );
            return response.Hits.Select(o => o.Source).ToList();
        }

        public IEnumerable<Book> Suggest(string search)
        {
            var result = ElasticClient.Search<Book>(x => x
                        .Suggest(s => s
                            .Completion("suggest", c => c
                                .Field(f => f.Suggest)
                                .Prefix(search)
                                .Fuzzy(f => f
                                    .Fuzziness(Fuzziness.Auto)
                                )
                               )
                            ).Size(100)
                            );

            return result.Suggest["suggest"].SelectMany(x => x.Options).Select(y => y.Source);
        }

        private void GetBooksSuggest(object sender, EventArgs e)
        {
            listBox1.DataSource = null;
            var listBooks = Suggest(comboBox1.Text);
            listBox1.DataSource = listBooks.ToList();
            listBox1.DisplayMember = nameof(Book.Title);
        }
    }
}
